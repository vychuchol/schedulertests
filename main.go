package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"sync/atomic"
)

const (
	URL      = "http://localhost/"
	APIURL   = URL + "api"
	LoginURL = URL + "login"
)

var lastCallID int64

func login() (*http.Cookie, error) {
	c := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	r, err := c.PostForm(LoginURL, url.Values{"username": {"admin"}, "password": {"admin"}})
	if err != nil {
		return nil, err
	}
	session := r.Cookies()[0]
	session.MaxAge = 0
	return session, nil
}

func call(session *http.Cookie, method string, parameters map[string]interface{}) (interface{}, error) {
	id := atomic.AddInt64(&lastCallID, 1)
	reqData := map[string]interface{}{
		"jsonrpc": "2.0",
		"method":  method,
		"params":  parameters,
		"id":      fmt.Sprintf("%d", id),
	}
	b, err := json.Marshal(reqData)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", APIURL, bytes.NewReader(b))
	req.AddCookie(session)
	if err != nil {
		return nil, err
	}
	c := &http.Client{}
	resp, err := c.Do(req)
	// TODO StatusCode
	respData := make(map[string]interface{})
	err = json.NewDecoder(resp.Body).Decode(&respData)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}
	respError, ok := respData["error"]
	if ok {
		return nil, fmt.Errorf("error returned: %v", respError)
	}
	return respData["result"], nil
}

func main() {
	fmt.Println("Scheduler Tests")
	session, err := login()
	if err != nil {
		fmt.Println(fmt.Errorf("can not log in: %v", err))
		os.Exit(1)
	}
	fmt.Println(call(session, "ping", map[string]interface{}{}))
	fmt.Println(call(session, "create_task", map[string]interface{}{
		"date_time":    "2018-06-04T10:15:00Z",
		"description":  "",
		"duration":     "PT30M",
		"employee_id":  "1",
		"name":         "Xxx",
		"task_type_id": "1",
	}))
}
