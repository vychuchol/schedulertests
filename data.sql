DELETE FROM action_logs;

DELETE FROM roles;

DELETE FROM tasks;

DELETE FROM employees;

DELETE FROM users;

INSERT INTO users (id, username, password) VALUES (1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918');

INSERT INTO employees (id, name) VALUES
    (1, 'Adam'),
    (2, 'Bára');

INSERT INTO roles (user_id, employee_id, name, seq) VALUES 
    (1, 1, 'editor', 1),
    (1, 2, 'editor', 2);
